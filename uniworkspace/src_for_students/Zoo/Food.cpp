#include "Food.hpp"
#include <string>

using namespace std;

Food::Food(const string& new_name, double new_cost, unsigned int new_energy) {
	_name = new_name;
	_cost = new_cost;
	_energy = new_energy;
	_quantity = 0;
}

string Food::getName() const {
	return _name;
}

double Food::getCost() const {
	return _cost;
}

unsigned int Food::getQuantity() const {
	return _quantity;
}

unsigned int Food::getEnergy() const {
	return _energy;
}

unsigned int Food::consume(unsigned int count) {
	if(count > _quantity) {
		count -= _quantity;
		_quantity = 0;
	}
	else {
		_quantity -= count;
	}
	return count;
}

void Food::purchase(unsigned int quantity) {
	_quantity += quantity;
}
