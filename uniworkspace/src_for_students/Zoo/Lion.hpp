#ifndef LION_HPP
#define LION_HPP

#include "Animal.hpp"

class Lion : public Animal {
public:
    Lion();

    
    // Overridden  so we can return AT_LION
    virtual AnimalType type() const;
    virtual void resetToHungry();
};

#endif /* end of include guard: LION_HPP */
