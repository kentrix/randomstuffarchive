#ifndef ELEPHANT_HPP
#define ELEPHANT_HPP

#include "Animal.hpp"

class Elephant : public Animal {
public:
    Elephant();

    // Overridden  so we can return AT_ELEPHANT
    virtual AnimalType type() const;
    // Overridden to allow resetting of the elephant's hunger at the end of the day
    virtual void resetToHungry();
};

#endif /* end of include guard: ELEPHANT_HPP */
