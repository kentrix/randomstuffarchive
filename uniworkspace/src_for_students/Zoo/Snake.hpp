#ifndef SNAKE_HPP
#define SNAKE_HPP

#include "Animal.hpp"

class Snake : public Animal {
public:
    Snake();

    // Overridden  so we can return AT_SNAKE
    virtual AnimalType type() const;
    // Overridden to allow resetting of the snake's hunger at the end of the day
    virtual void resetToHungry();

    /*
protected:
    unsigned int hunger_level;
    */
};

#endif /* end of include guard: SNAKE_HPP */
