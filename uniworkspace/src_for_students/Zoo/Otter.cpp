#include "Otter.hpp"

using namespace std;

Otter::Otter() {
	resetToHungry();
	_likedFood.push_back("fish");
	_likedFood.push_back("mouse");
}

Animal::AnimalType Otter::type() const {
    return AT_OTTER;
}


void Otter::resetToHungry() {
	_hungerLevel = 750;
}

