#include "Animal.hpp"


Animal::~Animal() {
}

Animal::AnimalType Animal::type() const {
    //Subclasses need to overwrite this method
    //to return the correct value
    return AT_INVALID;
}

unsigned int Animal::hungerLevel() const {
    //Provies a default implementation for all Animal subclasses
    //As all methods of the subclasses should have the same functionality
    return _hungerLevel;
}

void Animal::resetToHungry() {
    //Subclasses need to overwrite this method
    //to return the correct value
}

bool Animal::likesFood(const Food& food) const {
    //Provides a default impelementation for all Animal 
    //subclasses
    bool ret = false;
    for(std::string s : _likedFood) {
        if(!s.compare(food.getName())) {
            ret = true;
	    break;
	}
    }
    return ret;
}

unsigned int Animal::feed(Food& food) {
    //Provies a default implementation for all Animal subclasses
    //As all methods of the subclasses should have the same functionality
    if(!this->likesFood(food)) {
        return 0; 
    }
    unsigned int consumableQuantity;
    unsigned int consumed;
    consumableQuantity = std::ceil((double)_hungerLevel / food.getEnergy());
    consumed = food.consume(consumableQuantity);
    if(consumed * food.getEnergy() > _hungerLevel) {
    	_hungerLevel = 0;
    }
    else {
	    _hungerLevel -= consumed * food.getEnergy();
    }
    return consumed;


}
