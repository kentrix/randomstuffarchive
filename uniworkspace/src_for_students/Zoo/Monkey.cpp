#include "Monkey.hpp"

using namespace std;

Monkey::Monkey() {
	resetToHungry();
	_likedFood.push_back("banana");
	_likedFood.push_back("watermelon");
	_likedFood.push_back("mealworms");
}

Animal::AnimalType Monkey::type() const {
	return AT_MONKEY;
}

/*
unsigned int Monkey::hungerLevel() const {
	return _hungerLevel;
}
*/
void Monkey::resetToHungry() {
	_hungerLevel = 800;
}
/*
bool Monkey::likesFood(const Food& food) const {
    // TODO: Implement
    return false;
}

unsigned int Monkey::feed(Food& food) {
    // TODO: Implement
    return 0;
}
*/
