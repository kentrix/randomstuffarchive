#include "Snake.hpp"

using namespace std;

Snake::Snake() {
	resetToHungry();
	_likedFood.push_back("mouse");
	_likedFood.push_back("egg");
}

Animal::AnimalType Snake::type() const {
    return AT_SNAKE;
}

void Snake::resetToHungry() {
	_hungerLevel = 250;
}

