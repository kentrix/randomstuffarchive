#include "Elephant.hpp"

using namespace std;

Elephant::Elephant() {
	resetToHungry();
	_likedFood.push_back("banana");
	_likedFood.push_back("watermelon");
	_likedFood.push_back("hay");
}

Animal::AnimalType Elephant::type() const {
    return AT_ELEPHANT;
}


void Elephant::resetToHungry() {
	_hungerLevel = 8400;
}

