#include "ZooManagementSystem.hpp"

using namespace std;

ZooManagementSystem::ZooManagementSystem(double initialBalance) {
    _balance = initialBalance;
}

ZooManagementSystem::~ZooManagementSystem() {
    for(Animal* a : _animals) {
        //For each and delete the allocated Animal object
        delete a;
    }
}

string ZooManagementSystem::author() {
    //Trivial
    return "agil851";
}

double ZooManagementSystem::getBalance() const {
    //Return the currect balance
    return _balance;
}

double ZooManagementSystem::admitVisitors(unsigned int adults, unsigned int children) {
    int famPass;
    double cost;
    //Count the number of family pass to issue
    famPass = std::min((adults / 2), (children / 2));
    adults -= famPass * 2;
    children -= famPass * 2;
    //Calculate the total cost based on the number of familiy passes
    //and the remaining adults and children who were not covered by the pass
    cost = famPass * 30 + adults * 14 + children * 5;
    _balance += cost;
    return cost;
}

vector<Food> ZooManagementSystem::getFood() const {
    //Return the vector of stocked food
    return _stockedFood;
}

void ZooManagementSystem::addFood(const Food& new_food) {
    //Sanity check of the food to make sure its not already in there
    bool found = false;
    for(Food f : _stockedFood) {
        if(!f.getName().compare(new_food.getName())) {
            found = true;
            break;
        }
    }
    if(!found) {
        //its a new kind of food
        _stockedFood.push_back(new_food);
    }
}

bool ZooManagementSystem::purchaseFood(double& cost, unsigned int index, unsigned int quantity) {
    if(index >= _stockedFood.size()) {
        //Sanity check of the parameter `index`
        cost = 0.0;
        return false;
    }

    cost = _stockedFood[index].getCost() * quantity;
    if(cost > _balance) {
        //Not enough balance to purchase the food
        return false;
    }
    _balance -= cost;
    //Calls the purchase() method to increase the stock
    _stockedFood[index].purchase(quantity);
    return true;
}

vector<Animal*> ZooManagementSystem::getAnimals() const {
    //return the Vector of pointer to Animal objects
    return _animals;
}

bool ZooManagementSystem::addAnimal(Animal::AnimalType new_animal_type) {
    bool ret = true;
    //Use a case/switch to discriminate between different type of animals
    switch(new_animal_type){
        case Animal::AT_MONKEY:
            _animals.push_back(new Monkey());
            break;
        case Animal::AT_LION:
            _animals.push_back(new Lion());
            break;
        case Animal::AT_ELEPHANT:
            _animals.push_back(new Elephant());
            break;
        case Animal::AT_SNAKE:
            _animals.push_back(new Snake());
            break;
        case Animal::AT_OTTER:
            _animals.push_back(new Otter());
            break;
        case Animal::AT_INVALID:
        default:
            ret = false;
    }
    return ret;
}

ZooManagementSystem::FeedResult ZooManagementSystem::feedAnimal(unsigned int& quantity_eaten, unsigned int animal_index, unsigned int food_index) {
    quantity_eaten = 0;
    //Checks if any of the index are out of range.
    if(food_index >= _stockedFood.size()) {
        if(animal_index >= _animals.size()) {
            return FR_INVALID_ANIMAL;
        }
        return FR_INVALID_FOOD;
    }
    else if(animal_index >= _animals.size()) {
        return FR_INVALID_ANIMAL;
    }
    //Checks if the animal likes the food
    if(!_animals[animal_index]->likesFood(_stockedFood[food_index])) {
        //Dislikes
        return FR_INCOMPATIBLE;
    }

    //Find out the actual amount eaten by calling the feed() method on an Animal object
    quantity_eaten = _animals[animal_index]->feed(_stockedFood[food_index]);

    if(_animals[animal_index]->hungerLevel() == 0) {
        return FR_SUCCESS;
    }
    else{
        //Still hungry after the feed
        return FR_EXHAUSTED;
    }
}

void ZooManagementSystem::resetAllAnimals() {
    for(Animal* a : _animals) {
        a->resetToHungry();
    }
}

bool ZooManagementSystem::animalComp(Animal* animalA, Animal* animalB) {
    return (animalA->hungerLevel() > animalB->hungerLevel());
}

bool ZooManagementSystem::foodComp(Food* foodA, Food* foodB) {
    return (foodA->getQuantity() > foodB->getQuantity());
}

void ZooManagementSystem::feedAllAnimals() {
    //make a copy of the vector of pointer to the animals
    std::vector<Animal*> cp = _animals;
    std::vector<Food*> dp;

    //Make a vector of pointer to the food which allows simple
    //sorting with a comparator while allowing changes to be made 
    //to the Food objects
    for(unsigned int i = 0; i < _stockedFood.size(); i++) {
        dp.push_back(&(_stockedFood[i]));
    }
    //Sort(stable) both the pointers to animals and pointer to food 
    //without altering the order of the original vectors
    std::sort(cp.begin(), cp.end(), ZooManagementSystem::animalComp);
    std::sort(dp.begin(), dp.end(), ZooManagementSystem::foodComp);

    //For each animal, loop through the sorted vector of pointers of food
    //and attempt the feed the animal if the animal likes it
    for(Animal* a : cp) {
        for(Food* f : dp) {
            if(a->likesFood(*f)) {
                a->feed(*f);
            }
        }
    }

}
