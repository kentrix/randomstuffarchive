#ifndef OTTER_HPP
#define OTTER_HPP

#include "Animal.hpp"

class Otter : public Animal {
public:
    Otter();

    // Overridden  so we can return AT_OTTER
    virtual AnimalType type() const;
    // Overridden to allow resetting of the otter's hunger at the end of the day
    virtual void resetToHungry();
};

#endif /* end of include guard: ELEPHANT_HPP */
