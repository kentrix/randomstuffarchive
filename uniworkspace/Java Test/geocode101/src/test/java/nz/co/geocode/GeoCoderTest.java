package nz.co.geocode;

import static org.junit.Assert.assertEquals;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
/**
 *
 */
public class GeoCoderTest {

    private static final String API_KEY = "AIzaSyCOLmSrXnRFlNMMlZ5DXXvRXZlKgjoroGA";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testGeoCode() throws Exception {
        GeoCoder geoCoder = new GeoCoder(API_KEY);
        String[] geoCodeInfo = geoCoder.geoCode("Sydney");
        assertEquals("lattitude should be", "-33.8674869", geoCodeInfo[0]);
        assertEquals("longitude should be", "151.2069902", geoCodeInfo[1]);
    }

    @Test
    public void testGeoCodeNullApiKey() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("APIKey is mandatory.");
        GeoCoder geoCoder = new GeoCoder(null);
        geoCoder.geoCode("Sydney");
    }

    @Test
    public void testGeoCodeNullCity() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Address can`t be null or empty");
        GeoCoder geoCoder = new GeoCoder(API_KEY);
        String[] geoCodeInfo = null;
        geoCodeInfo = geoCoder.geoCode(null);

        assertEquals("lattitude should be", "-33.8674869", geoCodeInfo[0]);
        assertEquals("longitude should be", "151.2069902", geoCodeInfo[1]);
    }

    @Test
    public void testReverseGeoCode() throws NumberFormatException, Exception {
        GeoCoder geoCoder = new GeoCoder(API_KEY);
        String[] addressInfo = null;
        addressInfo = geoCoder.reverseGeoCode("40.714244", "-73.961452");
        assertEquals("line 1 should be", "277 Bedford Ave", addressInfo[0]);
        assertEquals("line 2 should be", " Brooklyn", addressInfo[1]);
        assertEquals("line State should be", "NY", addressInfo[2]);
        assertEquals("line Postcode should be", "11211", addressInfo[3]);
    }

    @Test
    public void testReverseGeoCodeLatisEmpty() throws NumberFormatException, Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("latitude can`t be null or empty");
        GeoCoder geoCoder = new GeoCoder(API_KEY);
        String[] addressInfo = null;
        addressInfo = geoCoder.reverseGeoCode("", "-73.961452");
        assertEquals("line 1 should be", "277 Bedford Avenue", addressInfo[0]);
        assertEquals("line 2 should be", " Brooklyn", addressInfo[1]);
        assertEquals("State should be", "NY", addressInfo[2]);
        assertEquals("Postcode should be", "11211", addressInfo[3]);
    }

    @Test
    public void testReverseGeoCodeLatisNull() throws NumberFormatException, Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("latitude can`t be null or empty");
        GeoCoder geoCoder = new GeoCoder(API_KEY);
        String[] addressInfo = null;
        addressInfo = geoCoder.reverseGeoCode(null, "-73.961452");
        assertEquals("line 1 should be", "277 Bedford Avenue", addressInfo[0]);
        assertEquals("line 2 should be", " Brooklyn", addressInfo[1]);
        assertEquals("line State should be", "NY", addressInfo[2]);
        assertEquals("line Postcode should be", "11211", addressInfo[3]);
    }

    @Test
    public void testReverseGeoCodeLongisNull() throws NumberFormatException, Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("longitude can`t be null or empty");
        GeoCoder geoCoder = new GeoCoder(API_KEY);
        String[] addressInfo = null;
        addressInfo = geoCoder.reverseGeoCode("40.714244", null);
        assertEquals("line 1 should be", "277 Bedford Avenue", addressInfo[0]);
        assertEquals("line 2 should be", " Brooklyn", addressInfo[1]);
        assertEquals("line State should be", "NY", addressInfo[2]);
        assertEquals("line Postcode should be", "11211", addressInfo[3]);
    }

    @Test
    public void testReverseGeoCodeLongisEmpty() throws NumberFormatException, Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("longitude can`t be null or empty");
        GeoCoder geoCoder = new GeoCoder(API_KEY);
        String[] addressInfo = null;
        addressInfo = geoCoder.reverseGeoCode("40.714244", "");
        assertEquals("line 1 should be", "277 Bedford Avenue", addressInfo[0]);
        assertEquals("line 2 should be", " Brooklyn", addressInfo[1]);
        assertEquals("line State should be", "NY", addressInfo[2]);
        assertEquals("line Postcode should be", "11211", addressInfo[3]);
    }

}
