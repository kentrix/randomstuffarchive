package nz.co.geocode;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

/**
 * This class provide basic GeoCoding functionalities.
 */
public class GeoCoder {

	private GeoApiContext geoContext;

	public GeoCoder(String apiKey) throws IllegalArgumentException {
		if(apiKey == null || apiKey.isEmpty()) {
			throw new IllegalArgumentException("APIKey is mandatory.");
		}

		this.geoContext = new GeoApiContext();
		geoContext.setApiKey(apiKey);
	}

	/**
	 * This method accepts an address/city and returns the latitude and
	 * longitude for the input address. This method returns a String array with
	 * two values holding the latitude and longitude values.
	 *
	 * @param address
	 *
	 * @return String [] latitudeAndLongitudeArray,
	 *         latitudeAndLongitudeArray[0], contains latitude for the address
	 *         latitudeAndLongitudeArray[1], contains longitude for the address
	 *
	 * @throws Exception,
	 *             IllegalArgumentException, in case of empty/null input.
	 *             Exception in case of an exception from upstream api.
	 */
	public String[] geoCode(String address) throws Exception {

		if(address == null || address.isEmpty()) {
			throw new IllegalArgumentException("Address can`t be null or empty");
		}
		// google api call
		GeocodingResult[] results = GeocodingApi.newRequest(geoContext).address(address).await();
		
		String[] ret = new String[2];
		ret[0] = Double.toString(results[0].geometry.location.lat);
		ret[1] = Double.toString(results[0].geometry.location.lng);

		return ret;
	}

	/**
	 * This method accepts the latitude and longitude of a place and returns the
	 * address/location for the input provided. This method returns a String
	 * array, with values as depicted below.
	 *
	 * @param latitude,
	 *            latitude for the location
	 * @param longitude,
	 *            longitude for the location
	 *
	 * @return String [] addressArray, addressArray[0], contains AddressLine1;
	 *         addressArray[1], contains City; addressArray[2], contains State;
	 *         addressArray[3], contains Postcode/Pincode/Zipcode;
	 *
	 * @throws NumberFormatException,
	 *             Exception NumberFormatException, in case of invalid latitude
	 *             and longitude values. IllegalArgumentException, in case of
	 *             empty/null input Exception, in case of an exception from the
	 *             upstream api.
	 *
	 */
	public String[] reverseGeoCode(String latitude, String longitude) throws NumberFormatException, Exception {
		if(latitude == null || latitude.isEmpty()) {
			throw new IllegalArgumentException("latitude can`t be null or empty");
		}
		if(longitude == null || longitude.isEmpty()) {
			throw new IllegalArgumentException("longitude can`t be null or empty");
		}
		
		double lat, lng;
		lat = Double.parseDouble(latitude);
		lng = Double.parseDouble(longitude);
		
		LatLng latlng = new LatLng(lat, lng);
		
		GeocodingResult[] results = GeocodingApi.newRequest(geoContext).latlng(latlng).await();
		
		String[] ret = new String[4];
		String[] tmp;
		tmp = results[0].formattedAddress.split(",");
		ret[0] = tmp[0];
		ret[1] = tmp[1];
		ret[2] = tmp[2].trim().split(" ")[0];
		ret[3] = tmp[2].trim().split(" ")[1];
		return ret;
	}

}