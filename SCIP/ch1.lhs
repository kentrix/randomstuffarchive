1.5

Haskell is lazy

> {-# LANGUAGE BangPatterns #-}
> module Main where

Function def

> a = sum [1..]

> fn n = if n == 0 then 0 else a
> fn' n = if n /= 0 then a else 0

Guard version

> fng n | n == 0 = 0
>       | otherwise = a


Define a new `if`

> newIf predicate first alt | predicate = first
>                 | otherwise = alt

> fnn n = newIf (n == 0) 0 a

Now we eval the alt first

> newIf' predicate first alt | not predicate = alt
>   | otherwise = first

> fnn' n = newIf' (n == 0) 0 a

A strict version of if will not terminate

> strictIf predicate !first !alt = if predicate then first else alt
> fns n = strictIf (n == 0) 0 a

All of them will terminate

> main = do 
>   putStrLn . show $ fn 0
>   putStrLn . show $ fng 0
>   print $ fnn 0
>   print $ fnn' 0
>   print $ fn' 0
>   print $ fns 0
