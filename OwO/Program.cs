﻿using System;
using System.Runtime.Serialization;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using Microsoft.Win32;
using System.Text.RegularExpressions;
using System.Threading;

namespace Exe
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                var rnd = new Random();
                int page = rnd.Next(0, 10);

                var client = new System.Net.WebClient();
                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                var s = client.DownloadString("https://e621.net/post.json?tags=ratio:1.5..1.7 order:score -female type:jpg type:png&limit=320&page=" + page);

                var regex = new Regex("\"file_url\":\"(.*?)\"", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                var col = regex.Matches(s);
                var list = new List<string>();

                foreach (Match item in col)
                {
                    var sp = item.ToString();
                    sp = sp.Split('\"')[3];
                    list.Add(sp);
                }

                var r = list[rnd.Next(list.Count())];
                Wallpaper.Set(new Uri(r));
                Thread.Sleep(333_000);
            }
        }
        public sealed class Wallpaper
        {
            Wallpaper() { }
            const int SPI_SETDESKWALLPAPER = 20;
            const int SPIF_UPDATEINIFILE = 0x01;
            const int SPIF_SENDWININICHANGE = 0x02;

            [DllImport("user32.dll", CharSet = CharSet.Auto)]
            static extern int SystemParametersInfo(int uAction, int uParam, string lpvParam, int fuWinIni);

            public enum Style : int
            {
                Tiled,
                Centered,
                Stretched
            }

            public static void Set(Uri uri, Style style = Style.Stretched)
            {
                System.IO.Stream s = new System.Net.WebClient().OpenRead(uri.ToString());
                var ext = uri.AbsolutePath.Split('.').Last;

                System.Drawing.Image img = System.Drawing.Image.FromStream(s);
                string tempPath = Path.Combine(Path.GetTempPath(), "i." + ext);
                img.Save(tempPath, System.Drawing.Imaging.ImageFormat.Bmp);

                RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Control Panel\Desktop", true);
                if (style == Style.Stretched)
                {
                    key.SetValue(@"WallpaperStyle", 2.ToString());
                    key.SetValue(@"TileWallpaper", 0.ToString());
                }

                if (style == Style.Centered)
                {
                    key.SetValue(@"WallpaperStyle", 1.ToString());
                    key.SetValue(@"TileWallpaper", 0.ToString());
                }

                if (style == Style.Tiled)
                {
                    key.SetValue(@"WallpaperStyle", 1.ToString());
                    key.SetValue(@"TileWallpaper", 1.ToString());
                }

                SystemParametersInfo(SPI_SETDESKWALLPAPER,
                    0,
                    tempPath,
                    SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);
            }
        }

    }
}

