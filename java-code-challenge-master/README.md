# Java Coding Challenge - Word-Art API

Our engineers have developed a state of the art Word-Art generator, using some
of the best open-source fonts from around the web to provide high quality images
from text for use on your community newsletters, group emails and board
presentation slides.  Your challenge is to build an API that uses HTTP to allow
our customers to request and fetch their images using our word art library.

You are free to spend as much time as you want on the challenge.  The key is to
send us something we can talk about in a further interview.

We have supplied a basic maven build script that can build a WAR file and run
the application with the jetty plugin.  Feel free to use it, or replace it with
something you feel comfortable using.  Just make sure we have instructions to run
it that can run on a recent version of debian based linux.

# Requirements

Your API, which you are free to design as you wish [1], must be able to accept a
string of text, along with formatting parameters, and be able to serve up the
generated images as PNG or JPEG images.

1 - Information about the formatting parameters can be found in the word art
    generator API javadocs

# CLI Test

A simple CLI is available to test the word art generation, it accepts only a
single parameter, the text, and outputs the result to stdout.  You can run it
with:

```
$ java -cp target/classes nz.net.catalyst.wordart.gen.CLI foo > foo.png
```

# ImageMagick

The given conversion code relies on the popular imagemagick library being installed
and available on your system's path.  You can check it is installed correctly by 
running `which convert`.

# A Word of Warning

One of our engineers was having a bad day while making major contributions to
this application.  There might be a few broken bits along the way.  Feel free
to bring these issues up with us, work around them, or fix them.