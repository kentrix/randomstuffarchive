Some thoughts about Elm
=======================

Recently, I have got around to try my hands around developing some
web-based application. Instead of going with the more tried and tested
route of Javascript plus Angular/React/Vue or the likes, I have decided
to try my hands on this small language called Elm.

Why did I go with Elm but not some other language which compile to Javascript?
Because it is
* Strong and static typed : It elimates a whole class of errors which are common amongst
  dynamic typed languages.
* Functional : Elm is a functional language, it has a very similar syntax to Haskell.
  Since I am learning haskell at the same time, I figured this could be a pretty good
  idea.

There is also another languaged called `Purescript` with even more similarity to plain
Haskell, I picked Elm because it is more mature at the moment.

Elm is great but it does have some down falls,
* The limitation of Elm architecture: At the time of writing, Elm uses the *Model, Subscription
  and Update* model. It is easy to understand and made sense for developing small to medium sized
  applications. However, as the application grows, its weakness starts to show; the type of message
  starts to grow.
  The is no way to update an element without updating the whole page.
* Awkward interops with Javascript: It was done via something called `Signal` in the 
  older versions of Elm, it has since then been replaced by the *Message and subscription*
  model. The interops with Javascript is quite limited still.
* Lack of language constructs: Elm does not come with Monads, of cause, one could always write 
  their own version. The is where I think Purescript has a leg up with the `Eff` Monads.
  In addition, the lack of `where` and 
* Uggh, JSON... : Dealing with JSON is still quite a nightmare, tons of boilerplate codes need to 
  be written to decode into something that Elm can recognise.
