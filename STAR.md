# 1

* S - A high profile government contractor came to us to implement a Audit system for a long term product, CabNet, a document manangement system for the paliament. However, the technogies used to build this project was quite outdated, and can no longer provide what the client wants. We are tasked with a rathered limited budget, which also implies a tight deadline.

* T - I am tasked to create the audit system which is suitable for the client's budget and provide a modern looking UI with a, maintainable architecture.

* A - I started off by suggesting new frameworks and languages we could use. Despite the old UI being made in 2012 technologies does not mean we are entirely stuck with it. If there is a way of us delivering the project on time and meeting all the targets, we have to think of something creative. I took the lead and offered using React for front-end as this is a quite standalone part of the application . With ample experience in React myself and some similar background in my team, I am sure we could deliver the project sucessfully.

* R - We deliver the projects with a standalone react front-end with modified and improved backend to pay off some of the technical debt. The UI was mordern and snappy. The clients were extermely satisfied with end-results and had been I kept hearing praises from them with regarding how the project was unblievely useful for them when dealing with leaks and other requests.

Customer Obesssion. Ownership. Invent and simplify. Insist on the highest standards

# 2

* S - I was assigned to a project team as their lead DevOps engineer and architect the infracture on AWS with terraform. It is a quite high profile project within the orgnisation and the lead dev who had the most knowldge just left the org. When I went in, I realised most of the team members are clusless about infrasturture. And something has to be done to push it forward.

* T - I need to design and architect the hosting platform and required services on AWS, however, inputs from the devs are crucial as part of my job. The most senior dev on the team has a slight introduction to AWS and eager to move along deployment with the code he currently have.

* A - I look thru the code we currently have, it was fragmented and copy pasted everywhere. The first thing I did was just to restrure and refactor the infrasture as code into composable and reuseable modules. Because as we are moving more and more porjects onto AWS, it will be even hard to manage. I've decided to have a video chat with the senior dev and explain all the details of why the prior works are needed before the project and go along.

* R - After lots of back and forth, video calls and slack chats, I managed to convice him that is best for the team and the wider org to complete this work. I worked with him closely for deployment and testing and he was quite satified with the outcome. As of now, we are all starting to use the restrutured modules to migrate more of our work to Terraform/AWS.

